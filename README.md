# Saboteur #

## Autorzy ##
* Olaf Dygas
* Radosław Panuszewski
* Anna Yelkina

## Zadanie ##
PW.W.4. Napisać grę podobną do gry Saboteur, gdzie jeden z graczy oszukuje tzn. jest sabotażystą, a reszta się domyśla patrząc na akcje graczy, kto jest sabotażystą. Zastosować model Kripkego do modelowania stanów przekonań graczy o swojej i innych roli w poszczególnych etapach gry.
Opracować algorytmy nagród i kar za akcje w grze wynikających ze stanów Kripkego graczy.
Gra powinna posiadać interfejs graficzny, podgląd na modele Kripkego każdego z graczy oraz ich akcje.

## Wygląd gry ##
![Alt text](https://i.ibb.co/nBpVY4h/Wygl-d-gry-Saboteur.png)
* Plansza
* Ręka aktualnego gracza 
* Modele Kripkiego dla każdego gracza
* Zielonym kolorem podświetlają się modele Kripkriego gracza, który aktualnie zagrywa
* Czerwony pasek obok modeli Kripkiego wskazuje na to, że gracz jest zablokowany.


## Opis gry Saboteur ##
“Gracze wcielają się w rolę kopaczy poszukujących bryłek złota w głębokich tunelach wijących się w trzewiach góry lub w sabotażystów, którzy na każdym kroku utrudniają pracę kopaczom. Członkowie obu grup powinni wspierać się wzajemnie, kiedy już odkryją, kto jest po ich stronie. Jeżeli kopacze zdołają przebić tunel do skarbu, otrzymują nagrodę w postaci bryłek złota, podczas gdy sabotażyści odchodzą z pustymi rękoma. Jednak, jeśli kopacze poniosą porażkę, nagrodę zgarniają sabotażyści.”

## Opis projektu ##
Projekt polega na zrobieniu symulacji gry z trzema botami sterowanymi komputerem. Zadaniem botów jest podejmowanie akcji zgodnie ze swoją rolą, a także podejmowanie wniosków dotyczące modelu świata rozgrywki na podstawie ruchów innych graczy.
Zachowania botów są realizowane za pomocą algorytmów zagrania kolejnych kart dla danej roli gracza.
Wnioskowanie jest realizowane przy zastosowaniu modelu Kripkiego dla każdego gracza. 

## Wybrane narzędzia ##
* C#
* Unity

## Założenia ##
Trzech graczy
Plansza do gry ma wymiary 16 na 9. W każdym polu może znajdować się karta tuneli przy spełnieniu odpowiednich warunków;
Talia do gry składa się kart:
50 kart tuneli, a dodatkowo karta startu i trzy karty końca,
16 kart ślepych zaułków,
7 kart blokujących,
7 kart odblokowujących,
3 karty zniszczenia,
Początkowa odległość od złota jest 22
Karta początkowa znajduje się w lewym dolnym rogu, złoto - w prawym górnym

## Sterowanie ##
Spacja - zmiana gracza.
Kolejna spacja - wykonanie ruchu przez aktualnego gracza.
Enter - przełączenie do trybu automatycznego.

## Model Kripkiego ##
Każdy gracz ma różne wagi zmiany poglądów (alfa/beta)
Wypisujemy w modelach Kripkiego przekonania aktualnego gracza o świecie oraz co on myśli, że myślą inni na temat świata (SMM, MSM lub MMS). Zapis MMS 0.5 pokazuje, że gracz z prawdopodobieństwem 50% wierzy w to, że modelem danej rozgrywki jest MMS (pierwszy oraz drugi gracze są kopaczami, trzeci gracz to sabotażysta).
Jeżeli przekonania o jakimś świecie przekraczają jakąś ustaloną wartość (np 60%), to gracz wierzy, że taki jest świat i zachowuje się zgodnie z przekonaniami. Np. jeżeli trzeci gracz (Kopacz) myśli, że Sabotażystą jest drugi gracz, to blokuje go.
Sabotażysta rozważa co inni o nim myślą i zachowuje się zgodnie z poglądami innych graczy. Tzn jeżeli on wierzy, że inni myślą, że jest sabotażystą, to wykonuje “dobry ruch” póki poglądy innych się nie zmienią, albo cel nie będzie za blisko. Jeżeli wierzy, że inni nie wiedzą, że jest sabotażystą, to wykonuje “zły ruch”.
Po każdym wykonaniu ruchu gracz analizuje jak jego ruch mógł być odebrany przez innych i odpowiednio zmienia swoje przekonania o przekonaniach innych.
Ze względu na to, że światy są między sobą związane, przy zmianie jednego świata zmienia się wiara w inne światy (np zwiększenie wiary w MMS powoduje zmniejszenie wiary w inne światy) . 
Gracz nie zmienia swoich przekonań o świecie, który od razu uważa za niemożliwy. Np. jeżeli gracz jest kopaczem, to przez całą grę jego przekonania o świecie, w którym on jest sabotażystą są równe zero.
Wartość, o jaką zmienia się każda z wag została zdefiniowana w następujący sposób 
* zwiększenie wagi – kiedy gracz zagrywa zgodnie z przekonaniami agenta o nim:
𝑇𝐹𝑛=𝑇𝐹𝑛−1+(1−𝑇𝐹𝑛−1)*𝛼*k
* zmniejszenie wagi – kiedy gracz zagrywa przeciwnie do przekonań agenta o nim:
𝑇𝐹𝑛=𝑇𝐹𝑛−1+(𝑇𝐹𝑛−1)*𝛽*k

gdzie:
𝑇𝐹𝑛 – waga krawędzi po ruchu gracza,
𝑇𝐹𝑛−1 – waga krawędzi przed akcją gracza,
𝛼 – współczynnik wzrostu przekonań,
𝛽 – współczynnik osłabienia przekonań.
k - współczynnik zmiany wagi.

Współczynnik zmiany wagi - na wartość zmiany wagi także wpływa oceniony przez innych graczy poziom dobrego ruchu. Tzn jeżeli gracz zniszczył dobry tunel, to wiara w to, że jest sabotażystą zwiększa się o wiele bardziej, niż jeżeli gracz zablokuje innego kopacza.

## Algorytmy ##
Dla prawidłowego zachowanie botów są zastosowane różne algorytmy, które są zastosowane w różnych funkcjach. Na przykład:
Klasa MinerBotViewModel, metoda Play() zawiera ogólny algorytm zachowania kopacza.
Klasa SaboteurBotViewModel, metoda Play() zawiera ogólny algorytm zachowania sabotażysty.
Klasa BotViewModel, metoda FindGoodTunnel() zawiera algorytm znajdowania tunelu, który można położyć na znalezione pole.
 Także każdy rodzaj botów ma swoja metodę wyboru karty do zrzucenia.

Poniżej są przedstawione dwa schematy algorytmów botów.

Algorytm decyzyjny kopacza:

![Alt text](https://i.ibb.co/fSD6rzK/Algorytm-decyzyjny-kopacza.jpg)

Algorytm decyzyjny sabotażysty:

![Alt text](https://i.ibb.co/52YDysp/Algorytm-decyzyjny-sabota-ysty.jpg)

## Klasy ##
W projekcie zastosowaliśmy wzorzec MVVM. Przy opisie klas ograniczymy się do warstwy ViewModel, gdyż działanie View jest trywialne i nie ma wpływu na samą logikę projektu.

PlayerViewModel - Klasa zawierająca logikę gracza. Dziedziczą po niej SaboteurViewModel oraz MinerViewModel, które doprecyzowują zasady działania “sztucznej inteligencji”, dla tych przypadków.Pla

KripkeViewModel - reprezentuje przekonania o świecie gry danego gracza. Nasz kontekst zawiera 3 “Światy Gry”, kolejno SMM, MSM, MMS, gdzie pozycja S oznacza numer gracza, który według tego przekonania jest sabotażystą. Wartości przekonań w światach gry zmieniają się po każdej zakończonej turze któregoś z graczy.

HandViewModel - Reprezentuje zbiór kart, które znajdują się w posiadaniu danego gracza. To z poziomu tego zbioru możemy karty zagrywać oraz do niego dobierać.

CardViewModel - Reprezentuje pojedynczą kartę. Dziedziczą po niej TunnelCardViewModel, FinishCardViewModel, BlockCardViewModel, UnblockCardViewModel, RuinCardViewModel. Każda z klas dziedziczących zawiera implementację logiki użycia karty na swój sposób. 

DeckViewModel - Reprezentuje talię kart, jest to zbiór kart, z którego dobieramy pojedyncze karty, które dodajemy do ręki któregoś z graczy.

FieldViewModel - Reprezentuje pojedyncze pole na planszy. Na polu można postawić kartę.

GameboardGridViewModel - Reprezentuje planszę gry, zawiera wszelkie potrzebne metody do określenia czy daną kartę da się postawić na danym polu, lub do wyszukiwania pól, na których położenie karty jest możliwe, wyszukiwanie odległości pomiędzy polami oraz wiele innych pomocnych przy przeszukiwaniu planszy funkcji.


## Wnioski ##
Przy zmianie parametrów alfa i beta, przypisanych do graczy można zaobserwować, że kopaczy inaczej analizują ruchy sabotażysty. Przy “pozytywnym” podejściu (duże alfa, małe beta) kopaczy dłużej nie są pewni, że ktoś jest sabotażystą. Jeżeli obaj kopaczy mają pozytywne podejście sabotażysta wygrywa częściej, niż kopacze.
Przy “negatywnym” podejściu (małe alfa, duże beta) kopaczy szybciej przekonują się, że jeden gracz jest sabotażystą, i później wolniej zmieniają swoje przekonania. Przy dwóch “negatywnych” kopaczach sabotażysta częściej przegrywa.
Przy jednym “pozytywnym” kopaczu i jednym “negatywnym” można powiedzieć, że walka jest w miarę wyrównana - mogą wygrać jak kopacze, jak sabotażysta w zależności od wylosowanych kart.
Można powiedzieć, że zaproponowane algorytmy dla kopaczy i sabotażystów są dosyć dobre, ponieważ widzimy, że przy różnych rozkładach mogą wygrywać jak jedni, tak i drudzy.
Stosowane alfa-beta:
Gracz pozytywny: alfa = 0.13, beta = 0.05
Gracz neutralny: alfa = 0.07, beta = 0.07
Gracz negatywny: alfa = 0.05, beta = 0.13