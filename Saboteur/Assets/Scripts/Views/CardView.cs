﻿using UnityEngine;
using Saboteur;
using UniRx;

[RequireComponent(requiredComponent: typeof(MouseFollower))]
public class CardView : MonoBehaviour
{
    #region Unity Fields
    
    [SerializeField] 
    private SpriteRenderer cardSprite;

    #endregion

    Vector3 positionBeforeDrag;

    public static bool IsDraggingAllowed { get; set; } = true;

    public CardViewModel ViewModel
    {
        get => Card;
        set => Card = value;
    }

    private CardViewModel Card { get; set; }

    private MouseFollower mouseFollower;

    public void Start ()
    {
        mouseFollower = GetComponent<MouseFollower>();
	}

    public void SetViewModel(CardViewModel vm)
    {
        ViewModel = vm;
        cardSprite.sprite = CardViewCreator.instance.GetSpriteFor(vm);
        if (cardSprite.sprite == null)
        {
            cardSprite.sprite = CardViewCreator.instance.GetSpriteForTunnel(((TunnelCardViewModel)vm).TunnelPattern.GetFlipped());
            cardSprite.flipY = true;
            cardSprite.flipX = true;
        }
        vm.CardRemovedStream.Subscribe(_ => RemoveCard());
    }

    private void Use(CardTargetView target)
    {
        //TODO sprawdzić na którym awatarze gracza albo na którym polu karta została upuszczona i podać jako target
        Card.Use(null, target.getCardTarget());
    }

    void ReturnToOriginPosition()
    {
        transform.position = positionBeforeDrag;
    }

    private void OnMouseDown()
    {
        //Debug.Log("Tunnel: " + ((TunnelCardViewModel) ViewModel).TunnelPattern.ToString());
        if (IsDraggingAllowed)
        {
            positionBeforeDrag = transform.position;
            mouseFollower.Follow(true);
        }
    }

    private void OnMouseUp()
    {
        if (IsDraggingAllowed)
        {
            CardTargetView target = MouseTargeter.instance.GetTargetUnderMouse();

            if (target != null)
                Use(target);
            else
                ReturnToOriginPosition();

            mouseFollower.Follow(false);
        }
    }

    private void RemoveCard()
    {
        if(this!=null)
        Destroy(gameObject);
    }
}
