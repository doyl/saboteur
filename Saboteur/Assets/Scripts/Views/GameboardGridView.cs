﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Saboteur;

public class GameboardGridView : MonoBehaviour
{
    private List<FieldView> fields = new List<FieldView>();

    [SerializeField] private GameObject fieldPrefab;

    [SerializeField] private Transform leftUpLimit;
    [SerializeField] private Transform rightDownLimit;

    [SerializeField] private int rows;
    [SerializeField] private int columns;

    private GameboardGridViewModel vm;

    public void Initialize()
    {
        SetViewModel(GameManager.GameboardGrid);
        GenerateGrid();
    }

    private void SetViewModel(GameboardGridViewModel vm)
    {
        this.vm = vm;
    }

    private void AddField(FieldView field, int x, int y)
    {
        fields.Add(field);
        field.VM.X = x;
        field.VM.Y = y;
        vm.AddField(field.VM);
    }

    public void GenerateGrid()
    {
        float xDistance = rightDownLimit.position.x - leftUpLimit.position.x;
        float yDistance = leftUpLimit.position.y - rightDownLimit.position.y;

        float xFieldSize = xDistance / columns;
        float yFieldSize = yDistance / rows;

        float firstFieldX = leftUpLimit.position.x + xFieldSize / 2;
        float firstFieldY = leftUpLimit.position.y - yFieldSize / 2;

        for(int i=0; i<rows; ++i)
        {
            float yPosition = firstFieldY - yFieldSize * i;
            for (int j = 0; j < columns; ++j)
            {
                float xPosition = firstFieldX + xFieldSize * j;
                Vector2 position = new Vector2(xPosition, yPosition);
                GameObject instance = Instantiate(fieldPrefab, position, Quaternion.identity, transform);
                AddField(instance.GetComponent<FieldView>(), j, i);
            }
        }
    }

}
