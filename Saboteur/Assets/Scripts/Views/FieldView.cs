﻿using System;
using System.Collections;
using System.Collections.Generic;
using Saboteur;
using UniRx;
using UnityEngine;

public class FieldView : CardTargetView
{
    public FieldViewModel VM { get; private set; }
    private CardView occupiedCardView;

    private IDisposable cardPlacedDisposable;
    private IDisposable cardRemovedDisposable;

    private void Awake()
    {
        SetViewModel(new FieldViewModel());
        cardPlacedDisposable = VM.CardPlacedStream.Subscribe(PutCard);
        cardRemovedDisposable = VM.CardRemovedStream.Subscribe(RemoveCard);
    }

    private void PutCard(CardViewModel card)
    {
        occupiedCardView = CardViewCreator.instance.GetCardView(card);
        if(occupiedCardView!=null)
            occupiedCardView.transform.position = transform.position;
    }

    private void RemoveCard(Unit unit)
    {
        if (occupiedCardView == null) return;
        Destroy(occupiedCardView.gameObject);
    }

    private void SetViewModel(FieldViewModel vm)
    {
        this.VM = vm;
    }

    public override ICardTarget getCardTarget()
    {
        return VM;
    }

    private void OnDestroy()
    {
        cardPlacedDisposable.Dispose();
        cardRemovedDisposable.Dispose();
    }
}
