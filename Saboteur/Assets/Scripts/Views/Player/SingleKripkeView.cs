﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Saboteur;
using UniRx;

public class SingleKripkeView : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI smm;
    [SerializeField] TextMeshProUGUI msm;
    [SerializeField] TextMeshProUGUI mms;

    public KripkeViewModel VM { get; set; }

    public void SetViewModel(KripkeViewModel vm)
    {
        VM = vm;
        vm.BeliefsChangedObservable.Subscribe(SetKripke);
    }

    public void SetKripke(List<GameWorld> worlds)
    {
        SetSMM(worlds[0].Belief);
        SetMSM(worlds[1].Belief);
        SetMMS(worlds[2].Belief);
    }

    public void SetSMM(float value)
    {
        smm.text = "SMM - " + Mathf.Round(value * 100) / 100;
    }

    public void SetMSM(float value)
    {
        msm.text = "MSM - " + Mathf.Round(value * 100) / 100;
    }

    public void SetMMS(float value)
    {
        mms.text = "MMS - " + Mathf.Round(value * 100) / 100;
    }
}
