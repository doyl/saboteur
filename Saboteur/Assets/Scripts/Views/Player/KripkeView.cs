using UnityEngine;
using TMPro;
using System.Collections.Generic;

namespace Saboteur
{
    public class KripkeView: MonoBehaviour
    {
        [SerializeField] SingleKripkeView[] kripkes;
        int PlayerNumber { get; set; } 

        public void SetKripkeForPlayer(int player, KripkeViewModel vm)
        {
            if(vm!=null)
            kripkes[player].SetViewModel(vm);
        }

        public void SetWorldForPlayer(int player, List<GameWorld> worlds)
        {
            kripkes[player].SetSMM(worlds[0].Belief);
            kripkes[player].SetMSM(worlds[1].Belief);
            kripkes[player].SetMMS(worlds[2].Belief);
        }
    }
}