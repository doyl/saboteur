using UniRx;
using UnityEngine;
using static Saboteur.TurnPhase;
using UnityEngine.UI;

namespace Saboteur
{
    public class PlayerView: MonoBehaviour
    {
        [SerializeField] private TurnManager turnManager;
        
        [SerializeField] private HandView handView;
        [SerializeField] private KripkeView kripkeView;
        
        [SerializeField] private bool isBot;
        [SerializeField] private int playerNumber;


        [SerializeField] private Image blockedImage;

        [SerializeField] private Image playerImage;
        [SerializeField] private Color myTurnColor;
        [SerializeField] private Color notMyTurnColor;
        

        private PlayerViewModel player;

        public void Initialize()
        {
            bool isSaboteur = playerNumber == GameManager.SaboteurNumber;
            
            if (isBot)
            {
                if (isSaboteur)
                    player = new SaboteurBotViewModel(playerNumber);
                else
                    player = new MinerBotViewModel(playerNumber);
            }
            else
                player = new HumanPlayerViewModel(isSaboteur, playerNumber);

            player.BlockedStream.Subscribe(SetBlocked);

            GameManager.Players[playerNumber] = player;

            GameManager.GameboardGrid.PlayerWonStream.Merge(turnManager.PlayerWonStream).Subscribe(player =>
            {
                Debug.Log($"Player {player.PlayerNumber} has won!");
            });
            
            turnManager.RegisterPlayer(player);

            turnManager.NextTurnStream.Subscribe(turn =>
            {
                if (turn.Phase == AnalysisPhase) 
                {
                    if (playerNumber != turn.LastActedPlayerNumber && turn.LastPlayedCard != null)
                    {
                        (player as BotViewModel)?.AnalyzePlayersAction(turn.LastPlayedCard, turn.LastActedPlayerNumber);
                    }

                    SetNotMyTurn();

                    if (playerNumber == turn.CurrentActingPlayerNumber)
                    {
                        SetMyTurn();
                        handView.ViewModel = player.Hand;
                        Debug.Log($"Gracz {playerNumber} " + ((player.IsSaboteur) ? "(Sabotażysta)" : "(Kopacz)") + " będzie teraz wykonywał ruch." +
                                  $" Ma kart {player.Hand.Cards.Count}, a wyświetlanych jest {handView.cardViews.Count}.");
                    }
                }

                if (turn.Phase == GamePhase)
                {
                    if (playerNumber == turn.CurrentActingPlayerNumber)
                    {
                        player.BeginTurn();
                    }
                }
            });

            if (kripkeView == null) return;
            kripkeView.SetKripkeForPlayer(((playerNumber + 2) % 3), player.WhatIThinkRightPlayerThinks);
            kripkeView.SetKripkeForPlayer(((playerNumber + 1) % 3), player.WhatIThinkLeftPlayerThinks);
            kripkeView.SetKripkeForPlayer(((playerNumber) % 3), player.WhatIThink);
        }

        private void Update()
        {
            if (isBot) return;

            HumanPlayerViewModel humanPlayer = (HumanPlayerViewModel) player;
            
            if (Input.GetKeyDown(KeyCode.D) && player.Hand.Cards.Count < 6)
            {
                humanPlayer.TakeCard();
            }
        }

        private void SetBlocked(bool blocked)
        {
            blockedImage.enabled = blocked;
        }
        

        private void SetMyTurn()
        {
            playerImage.color = myTurnColor;
        }

        private void SetNotMyTurn()
        {
            playerImage.color = notMyTurnColor;
        }
    }
}