﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Saboteur
{
    public class HandView : MonoBehaviour
    {
        #region Unity Fields

        [SerializeField] private Transform handStartPoint;
        [SerializeField] private float horizontalDistanceBetweenCards;
        [SerializeField] private GameObject cardPrefab;

        #endregion

        public readonly List<CardView> cardViews = new List<CardView>();
        private HandViewModel hand;
        private IDisposable cardTakenDisposable;
        private IDisposable cardRemovedDisposable;

        public HandViewModel ViewModel
        {
            get => hand;
            set
            {
                cardTakenDisposable?.Dispose();
                cardRemovedDisposable?.Dispose();
                hand = value;
                cardTakenDisposable = hand.CardTakenStream.Subscribe(AddCard);
                cardRemovedDisposable = hand.CardRemovedStream.Subscribe(RemoveCard);
                
                foreach (CardView cardView in cardViews)
                {
                    Destroy(cardView.gameObject);
                }
                cardViews.Clear();
                
                foreach (CardViewModel card in hand.Cards)
                {
                    AddCard(card);
                }
                MoveCards();
            }
        }

        private void AddCard(CardViewModel card)
        {
            CardView cardView = Instantiate(cardPrefab, transform).GetComponent<CardView>();
            cardView.SetViewModel(card);
            cardView.transform.position = GetPositionForIndex(cardViews.Count);
            cardViews.Add(cardView);
        }

        private void RemoveCard(CardViewModel card)
        {
            CardView cardView = cardViews.Find(x => x.ViewModel == card);
            if(cardView!=null)
            Destroy(cardView.gameObject);
            cardViews.Remove(cardView);
            MoveCards();
        }

        private void MoveCards()
        {
            for (int i = 0; i < cardViews.Count; ++i)
            {
                cardViews[i].transform.position = GetPositionForIndex(i);
            }
        }

        private Vector3 GetPositionForIndex(int index)
        {
            Vector3 position = handStartPoint.position + new Vector3(index * horizontalDistanceBetweenCards, 0, 0);
            return position;
        }
    }
}