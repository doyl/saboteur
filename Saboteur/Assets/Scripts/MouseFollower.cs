﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseFollower : MonoBehaviour
{
    private Vector3 mousePosition;
    [SerializeField] private float moveSpeed = 0.5f;

    bool shouldFollow = false;

    private void Update()
    {
        if (!shouldFollow) return;

        FollowMouse();
    }

    private void FollowMouse()
    {
        mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        transform.position = Vector2.Lerp(transform.position, mousePosition, moveSpeed);
    }

    public void Follow(bool shouldFollow)
    {
        this.shouldFollow = shouldFollow;
    }
}
