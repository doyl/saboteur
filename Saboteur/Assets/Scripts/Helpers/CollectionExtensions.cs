using System.Collections;
using System.Collections.Generic;

namespace Saboteur
{
    public static class CollectionExtensions
    {
        public static void AddIfNotNull<T>(this ICollection<T> collection, T value) 
        {
            if (value != null) { collection.Add(value); }
        }

        public static void RemoveIfContains<T>(this ICollection<T> collection, T value)
        {
            if (collection.Contains(value)) collection.Remove(value);
        }
    }
}