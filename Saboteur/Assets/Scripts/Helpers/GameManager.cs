using System;
using System.Collections.Generic;
using UniRx;
using Random = UnityEngine.Random;

namespace Saboteur
{
    public class GameManager
    {
        public static Dictionary<int, PlayerViewModel> Players { get; private set; } = new Dictionary<int, PlayerViewModel>();
        public static DeckViewModel Deck { get; private set; } = new DeckViewModel();
        public static GameboardGridViewModel GameboardGrid { get; } = new GameboardGridViewModel();
    //    public static int SaboteurNumber { get;  } = Random.Range(0, 3);
        public static int SaboteurNumber { get;  } = 0;
        public static CardViewModel LastPlayedCard { get; set; }
        public static CardViewModel StartCard { get; set; }
        public static CardViewModel FinishCard { get; set; }

        public static void Restart()
        {
            Deck = new DeckViewModel();
            foreach(PlayerViewModel player in Players.Values)
            {
                player.Hand.Clear();
                player.TakeAllCards();
                ((BotViewModel)player).SetKripkeModels(player.IsSaboteur);
            }

            GameboardGrid.Clear();
            TurnManager.instance.SetInitialCards();
        }

    }
}