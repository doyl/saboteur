using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using static Saboteur.Present;
using static Saboteur.TurnPhase;

namespace Saboteur
{
    public class TurnManager: MonoBehaviour
    {
        #region Streams

        private Subject<Turn> nextTurnSubject = new Subject<Turn>();
        public IObservable<Turn> NextTurnStream => nextTurnSubject.AsObservable();
        
        private static Subject<PlayerViewModel> playerWonSubejct = new Subject<PlayerViewModel>();
        public IObservable<PlayerViewModel> PlayerWonStream => playerWonSubejct.AsObservable();

        #endregion
        
        public const int PLAYER_COUNT = 3;
        
        public static TurnManager instance;

        private Turn turn = new Turn();
        private bool firstTime = true;
        bool freeMode = false;
        [SerializeField] EndMenu endMenu;
        [SerializeField] GameboardGridView gameboardGird;
        [SerializeField] PlayerView[] players;
        
        public HashSet<PlayerViewModel> Players = new HashSet<PlayerViewModel>();

        public void RegisterPlayer(PlayerViewModel player)
        {
            Players.Add(player);
            
            player.CardUsedStream.Subscribe(card =>
            {
                turn.LastPlayedCard = card;
            });
        }

        private void Start()
        {
            instance = this;
            endMenu.Initialize();
            gameboardGird.Initialize();
            SetInitialCards();
            foreach(PlayerView player in players)
            {
                player.Initialize();
            }
        }

        public void SetInitialCards()
        {
            firstTime = false;

            FieldViewModel endField = GameManager.GameboardGrid.GetField(13, 1);
            CardViewModel finishCard = new FinishCardViewModel();
            GameManager.FinishCard = finishCard;
            finishCard.Use(null, endField);

            FieldViewModel beginField = GameManager.GameboardGrid.GetField(0, 8);
            CardViewModel beginCard = new BeginCardViewModel();
            GameManager.StartCard = beginCard;
            beginCard.Use(null, beginField);

            turn.LastPlayedCard = beginCard;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Return)) freeMode = !freeMode;

            if (Input.GetKeyDown(KeyCode.Space) || freeMode)
            { 
                int minersWithoutCards = Players.Count(p => !p.IsSaboteur && p.Hand.HasNoMoreCards());

                if (GameManager.Deck.HasNoMoreCards() && minersWithoutCards == PLAYER_COUNT - 1)
                {
                    PlayerViewModel saboteur = Players.First(p => p.IsSaboteur);
                    playerWonSubejct.OnNext(saboteur);
                }

                if (firstTime)
                {
                    turn.CurrentActingPlayerNumber = 0;
                    turn.Phase = AnalysisPhase;
                    firstTime = false;
                }
                else
                {
                    turn.Next();
                }

                if (turn.Phase == AnalysisPhase)
                {
                    Debug.Log("----------------FAZA ANALIZY----------------");
                    Debug.Log($"Ostatnio zagrana karta: {turn.LastPlayedCard}");
                }
                else
                    Debug.Log($"------------------FAZA GRY------------------ (Gracz: {turn.CurrentActingPlayerNumber})");

                nextTurnSubject.OnNext(turn);
            }
        }
    }
}