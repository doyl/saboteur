namespace Saboteur
{
    public enum PlayerPersonality
    {
        Positive,
        Neutral,
        Negative
    }
}