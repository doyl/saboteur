using UnityEngine;
using static Saboteur.TurnPhase;

namespace Saboteur
{
    public class Turn
    {
        public int CurrentActingPlayerNumber { get; set; }
        public int LastActedPlayerNumber => (CurrentActingPlayerNumber + 2) % 3;
        public TurnPhase Phase { get; set; }
        public CardViewModel LastPlayedCard { get; set; }
        
        
        public void Next()
        {
            if (Phase == AnalysisPhase) Phase = GamePhase;
            else
            {
                CurrentActingPlayerNumber = (CurrentActingPlayerNumber + 1) % TurnManager.PLAYER_COUNT;
                Phase = AnalysisPhase;
            }
        }
    }
}