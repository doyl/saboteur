namespace Saboteur
{
    public enum TurnPhase
    {
        AnalysisPhase,
        GamePhase,
    };
}