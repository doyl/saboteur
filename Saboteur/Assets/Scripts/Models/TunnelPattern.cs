using static Saboteur.Present;

namespace Saboteur
{
    public class TunnelPattern
    {
        public Present Up { get; set; }
        public Present Down { get; set; }
        public Present Left { get; set; }
        public Present Right { get; set; }
        public bool DeadEnd { get; set; }

        public TunnelPattern()
        {
            Up = DontCare;
            Down = DontCare;
            Left = DontCare;
            Right = DontCare;
        }

//        public bool IsEqualPattern(TunnelPattern other)
//        {            
//            if (other.Up == Yes &&    this.Up == No) return false;
//            if (other.Down == Yes &&  this.Down == No) return false;
//            if (other.Left == Yes &&  this.Left == No) return false;
//            if (other.Right == Yes && this.Right == No) return false;
//            
//            if (other.Up == No &&    this.Up == Yes) return false;
//            if (other.Down == No &&  this.Down == Yes) return false;
//            if (other.Left == No &&  this.Left == Yes) return false;
//            if (other.Right == No && this.Right == Yes) return false;
//
//            return true;
//        }

        public bool Matches(TunnelPattern other)
        {
            if (other == null) return false;
            
            bool upMatches = false;
            bool downMatches = false;
            bool leftMatches = false; 
            bool rightMatches = false;
            bool deadEndMatches = false;

            if (this.Up == other.Up || this.Up == DontCare || other.Up == DontCare) 
                upMatches = true;

            if (this.Down == other.Down || this.Down == DontCare || other.Down == DontCare)
                downMatches = true;

            if (this.Left == other.Left || this.Left == DontCare || other.Left == DontCare)
                leftMatches = true;

            if (this.Right == other.Right || this.Right == DontCare || other.Right == DontCare)
                rightMatches = true;

            if (this.DeadEnd == other.DeadEnd)
                deadEndMatches = true;

            return upMatches && downMatches && leftMatches && rightMatches && deadEndMatches;
        }

        public TunnelPattern GetFlipped()
        {
            TunnelPattern pattern = new TunnelPattern();
            pattern.Up = Down == Present.Yes ? Present.Yes : Present.No;
            pattern.Down = Up == Present.Yes ? Present.Yes : Present.No;
            pattern.Right = Left == Present.Yes ? Present.Yes : Present.No;
            pattern.Left = Right == Present.Yes ? Present.Yes : Present.No;

            pattern.DeadEnd = DeadEnd;
            return pattern;
        }

        public override string ToString()
        {
            return "" + Up.ToString() + " " + Down.ToString() + " " + Right.ToString() + " " + Left.ToString();
        }
    }
}