namespace Saboteur
{
    public enum CardType
    {
        Block,
        Unblock,
        Tunnel,
        Ruin,
        Begin,
        Gold
    };
}