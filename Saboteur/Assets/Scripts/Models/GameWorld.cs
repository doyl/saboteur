namespace Saboteur
{
    public class GameWorld
    {
        public float Belief { get; set; }
        public char[] ModelType { get; set; }
        public int SaboteurNumber { get; }
        
        public GameWorld(int saboteurNumber, float belief)
        {
            SaboteurNumber = saboteurNumber;
            Belief = belief;         
            ModelType = new char[3];
            
            for (int i = 0; i < 3; ++i)  // zamienic '3' na stala ilosc graczy
            {
                if (i == saboteurNumber)
                    ModelType[i] = 'S';
                else
                    ModelType[i] = 'M';
            }
        }

        public void ChangeBelief(float newBelief)
        {
            Belief += newBelief;
            
            if (Belief >= 1.0f)
                Belief = 1.0f;
            
            if (Belief <= 0.0f)
                Belief = 0.0f;
        }
    }
}