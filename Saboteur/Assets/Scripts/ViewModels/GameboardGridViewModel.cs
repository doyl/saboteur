using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Xml.Linq;
using UniRx;
using UnityEngine;
using static Saboteur.Present;
using static Saboteur.GameManager;

namespace Saboteur
{
    public class GameboardGridViewModel : ICardTarget
    {
        private Subject<PlayerViewModel> playerWonSubject = new Subject<PlayerViewModel>();
        public IObservable<PlayerViewModel> PlayerWonStream => playerWonSubject.AsObservable();

        private List<FieldViewModel> fields = new List<FieldViewModel>();
        private Dictionary<int, FieldViewModel> tunnelFields = new Dictionary<int, FieldViewModel>();
        private Dictionary<int, FieldViewModel> deadEnds = new Dictionary<int, FieldViewModel>();
        
        private static List<FieldViewModel> checkedFields = new List<FieldViewModel>();

        private int rows = 9;
        private int columns = 16;

        public FieldViewModel GetField(int x, int y)
        {
            if (x < 0 || x >= columns || y < 0 || y >= rows) return null;
            return fields[y * columns + x];
        }

        public FieldViewModel GetField(Vector2Int position)
        {
            return GetField(position.x, position.y);
        }
        
        public void AddField(FieldViewModel field)
        {
            fields.Add(field);
        }

        public List<FieldViewModel> GetAllPossibleFields()
        {
            List<FieldViewModel> list = GetPossibleFields(GameManager.StartCard.OccupiedField);
            checkedFields.Clear();
            return list;
        }

        public bool CanPlaceCardOnField(CardViewModel card, FieldViewModel field)
        {
            TunnelPattern pattern = ((TunnelCardViewModel)card).TunnelPattern;
            Vector2Int position = GetPositionOfField(field);

            FieldViewModel checkedField = GetField(position.x, position.y - 1);
            
            if(checkedField != null && !checkedField.isEmpty())
            {
                if (checkedField.OccupyingCard.Type == CardType.Gold) return true;
                
                TunnelPattern blockingPattern = ((TunnelCardViewModel)checkedField.OccupyingCard).TunnelPattern;
                if (blockingPattern.Down != pattern.Up) return false;
            }

            checkedField = GetField(position.x, position.y + 1);
            if (checkedField != null && !checkedField.isEmpty())
            {
                TunnelPattern blockingPattern = ((TunnelCardViewModel)checkedField.OccupyingCard).TunnelPattern;
                if (blockingPattern.Up != pattern.Down) return false;
            }

            checkedField = GetField(position.x - 1, position.y);
            if (checkedField != null && !checkedField.isEmpty())
            {
                
                TunnelPattern blockingPattern = ((TunnelCardViewModel)checkedField.OccupyingCard).TunnelPattern;
                if (blockingPattern.Right != pattern.Left) return false;
            }

            checkedField = GetField(position.x + 1, position.y);
            
            if (checkedField != null && !checkedField.isEmpty())
            {
                if (checkedField.OccupyingCard.Type == CardType.Gold) return true;
                
                TunnelPattern blockingPattern = ((TunnelCardViewModel)checkedField.OccupyingCard).TunnelPattern;
                if (blockingPattern.Left != pattern.Right) return false;
            }

            return true;
        }

        public List<FieldViewModel> GetListOfFieldsOpenedByPlacingCardOnField(CardViewModel card, FieldViewModel field)
        {
            List<FieldViewModel> fields = new List<FieldViewModel>();
            Vector2Int position = GetPositionOfField(field);
            TunnelPattern pattern = ((TunnelCardViewModel)card).TunnelPattern;

            if (pattern.Up == Present.Yes)
            {
                FieldViewModel checkedField = GetField(position.x, position.y - 1);
                if (checkedField != null)
                {
                    if (checkedField.OccupyingCard == null)
                        fields.Add(checkedField);
                }
            }

            if (pattern.Down == Present.Yes)
            {
                FieldViewModel checkedField = GetField(position.x, position.y + 1);
                if (checkedField != null)
                {
                    if (checkedField.OccupyingCard == null)
                        fields.Add(checkedField);
                }
            }

            if (pattern.Right == Present.Yes)
            {
                FieldViewModel checkedField = GetField(position.x + 1, position.y);
                if (checkedField != null)
                {
                    if (checkedField.OccupyingCard == null)
                        fields.Add(checkedField);
                }
            }

            if (pattern.Left == Present.Yes)
            {
                FieldViewModel checkedField = GetField(position.x - 1, position.y);
                if (checkedField != null)
                {
                    if (checkedField.OccupyingCard == null)
                        fields.Add(checkedField);
                }
            }

            return fields;
        }

        private List<FieldViewModel> GetPossibleFields(FieldViewModel startField)
        {
            List<FieldViewModel> possibleFields = new List<FieldViewModel>();

            if (startField == null) return null;
            if (checkedFields.Contains(startField)) return null;

            checkedFields.Add(startField);

            CardViewModel startCard = startField.OccupyingCard;
            if (startCard == null)
            {
                possibleFields.Add(startField);
                return possibleFields;
            }

            Vector2Int position = GetPositionOfField(startField);

            if(startCard is TunnelCardViewModel tunnel)
            {
                if (tunnel.TunnelPattern.DeadEnd) return null;

                if (tunnel.TunnelPattern.Up == Present.Yes)
                    MergeLists(possibleFields, GetPossibleFields(GetField(position.x, position.y - 1)));
                if (tunnel.TunnelPattern.Down == Present.Yes)
                    MergeLists(possibleFields, GetPossibleFields(GetField(position.x, position.y + 1)));
                if(tunnel.TunnelPattern.Right == Present.Yes)
                    MergeLists(possibleFields, GetPossibleFields(GetField(position.x+1, position.y)));
                if(tunnel.TunnelPattern.Left == Present.Yes)
                    MergeLists(possibleFields, GetPossibleFields(GetField(position.x - 1, position.y)));
            }

            return possibleFields;

        }

        private List<FieldViewModel> MergeLists(List<FieldViewModel> first, List<FieldViewModel> second)
        {
            if (second == null) return first;
            foreach(FieldViewModel f in second)
            {
                if (!first.Contains(f))
                    first.Add(f);
            }
            return first;
        }

        public void PutTunnel(FieldViewModel field, TunnelCardViewModel tunnel)
        {
            field.PutCard(tunnel);
            if (field == GameManager.FinishCard.OccupiedField) return;
            int distanceToGold = GetDistanceBetweenFields(field, GameManager.FinishCard.OccupiedField);
            tunnelFields[distanceToGold] = field;

            if (tunnel.TunnelPattern.DeadEnd)
                deadEnds[distanceToGold] = field;
            
            if (IsNextToGold(field))
                playerWonSubject.OnNext(tunnel.SourcePlayer);
        }

        public void RuinTunnel(FieldViewModel field)
        {
            field.RemoveCard();
            int key = tunnelFields.First(x => x.Value == field).Key;
            tunnelFields.Remove(key);
            deadEnds.Remove(key);
        }

        private bool IsNextToGold(FieldViewModel field)
        {
            TunnelPattern tunnel = (field.OccupyingCard as TunnelCardViewModel)?.TunnelPattern;

            if (tunnel == null) return false;

            if ((tunnel.Up == Yes && GetField(field.X, field.Y - 1)?.OccupyingCard == FinishCard)
                || (tunnel.Down == Yes && GetField(field.X, field.Y + 1)?.OccupyingCard == FinishCard)
                || (tunnel.Left == Yes && GetField(field.X - 1, field.Y)?.OccupyingCard == FinishCard)
                || (tunnel.Right == Yes && GetField(field.X + 1, field.Y)?.OccupyingCard == FinishCard))
                    return true;
            
            return false;
        }

        public FieldViewModel GetTunnelFieldClosestToGold()
        {
            return tunnelFields[tunnelFields.Keys.Min()];
        }

        public FieldViewModel GetDeadEndClosestToGold()
        {
            return deadEnds.Count > 0 ? deadEnds[deadEnds.Keys.Min()] : null;
        }

        public int GetMinDistanceToGold()
        {
            return tunnelFields.Keys.Min();
        }

        public List<FieldViewModel> GetListOfTunnelsClosestToGold()
        {
            List<FieldViewModel> fieldList = new List<FieldViewModel>();
            int minDistanceToGold = tunnelFields.Keys.Min();
            int maxDistanceToGold = minDistanceToGold < 20 ? minDistanceToGold + 10 : 23;
            
            for (int i = minDistanceToGold; i <= maxDistanceToGold; ++i)
            {
                foreach (var field in tunnelFields)
                {
                    if (field.Key == i)
                        fieldList.Add(field.Value);
                }
            }

            return fieldList;
        }

        public int GetMinimumDistanceByPlacingCard(FieldViewModel field, CardViewModel card)
        {
            TunnelPattern pattern = ((TunnelCardViewModel)card).TunnelPattern;
            Vector2Int position = GetPositionOfField(field);
            int minDistance = 999;
            int checkedDistance;

            if (pattern.Up == Present.Yes)
            {
                FieldViewModel checkedField = GetField(position.x, position.y - 1);
                if (checkedField != null)
                {
                    if (checkedField.OccupyingCard!= null && checkedField.OccupyingCard.Type == CardType.Gold) return 0;
                    if (checkedField.isEmpty())
                    {
                        checkedDistance = GetDistanceBetweenFields(checkedField, GameManager.FinishCard.OccupiedField);
                        if (checkedDistance < minDistance) minDistance = checkedDistance;
                    }
                }
            }
            if(pattern.Down == Present.Yes)
            {
                FieldViewModel checkedField = GetField(position.x, position.y + 1);
                if (checkedField != null)
                {
                    if (checkedField.OccupyingCard!= null && checkedField.OccupyingCard.Type == CardType.Gold) return 0;
                    if (checkedField.isEmpty())
                    {
                        checkedDistance = GetDistanceBetweenFields(checkedField, GameManager.FinishCard.OccupiedField);
                        if (checkedDistance < minDistance) minDistance = checkedDistance;
                    }
                }
            }

            if (pattern.Right == Present.Yes)
            {
                FieldViewModel checkedField = GetField(position.x + 1, position.y);
                if (checkedField != null)
                {
                    if (checkedField.OccupyingCard!= null && checkedField.OccupyingCard.Type == CardType.Gold) return 0;
                    if (checkedField.isEmpty())
                    {
                        checkedDistance = GetDistanceBetweenFields(checkedField, GameManager.FinishCard.OccupiedField);
                        if (checkedDistance < minDistance) minDistance = checkedDistance;
                    }
                }
            }

            if (pattern.Left == Present.Yes)
            {
                FieldViewModel checkedField = GetField(position.x - 1, position.y);
                if (checkedField != null)
                {
                    if (checkedField.OccupyingCard!= null && checkedField.OccupyingCard.Type == CardType.Gold) return 0;
                    if (checkedField.isEmpty())
                    {
                        checkedDistance = GetDistanceBetweenFields(checkedField, GameManager.FinishCard.OccupiedField);
                        if (checkedDistance < minDistance) minDistance = checkedDistance;
                    }
                }
            }
            return minDistance;
        }

        public Vector2Int GetPositionOfField(FieldViewModel field)
        {
            int position = fields.IndexOf(field);
            int xPos = position % columns;
            int yPos = position / columns;

            return new Vector2Int(xPos, yPos);
        }

        public int GetDistanceBetweenFields(FieldViewModel first, FieldViewModel second)
        {
            int firstPosition = fields.IndexOf(first);
            int firstXPos = firstPosition % columns;
            int firstYPos = firstPosition / columns;
            
            int secondPosition = fields.IndexOf(second);
            int secondXPos = secondPosition % columns;
            int secondYPos = secondPosition / columns;

            int xDif = Mathf.Abs(secondXPos - firstXPos);
            int yDif = Mathf.Abs(secondYPos - firstYPos);

            return xDif + yDif;
        }

        public void Clear()
        {
            tunnelFields.Clear();
            deadEnds.Clear();
            foreach(FieldViewModel field in fields)
            {
                field.Clear();
            }
        }
    }
}