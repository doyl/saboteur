using System;
using System.Collections.Generic;
using UnityEngine;
using static Saboteur.Present;
using static Saboteur.GameManager;

namespace Saboteur
{
    public abstract class BotViewModel : PlayerViewModel
    {
        protected int lastDistanceToGold;

        public static readonly float GoodAct = 1.5f;
        public static readonly float NeutralAct = 1;

        public BotViewModel(bool isSaboteur, int playerNumber) : base(isSaboteur, playerNumber)
        {
        }

        public void CreateKripkeModels(bool isSaboteur)
        {
            WhatIThink = new KripkeViewModel(this, PlayerNumber);
            WhatIThinkLeftPlayerThinks = new KripkeViewModel(this, GetLeftPlayerNumber());
            WhatIThinkRightPlayerThinks = new KripkeViewModel(this, GetRightPlayerNumber());
            SetKripkeModels(isSaboteur);
        }

        public void SetKripkeModels(bool isSaboteur)
        {
            WhatIThink.SetModel(true, isSaboteur);
            WhatIThinkLeftPlayerThinks.SetModel(false, isSaboteur);
            WhatIThinkRightPlayerThinks.SetModel(false, isSaboteur);
        }

        public abstract void AnalyzePlayersAction(CardViewModel card, int playerNumber);

        public override void BeginTurn()
        {
            CardView.IsDraggingAllowed = false;
            Play();
        }

        private void TakeCard()
        {
            Hand.TakeCard();
        }

        protected abstract void Play();

        protected bool HasCard(CardType cardType)
        {
            return Hand.HasCard(cardType);
        }

        protected bool HasTunnelCard(TunnelPattern tunnelPattern)
        {
            return Hand.HasTunnelCard(tunnelPattern);
        }

        protected void PlayUnblock(PlayerViewModel target, float isGoodAct)
        {
            Hand.PlayUnblock(target);
            ChangePossibleOthersBeliefsAboutMe(isGoodAct);
            Debug.Log($"Gracz {PlayerNumber} odblokowal gracza {target.PlayerNumber} ");
        }

        protected void PlayBlock(PlayerViewModel target, float isGoodAct)
        {
            Hand.PlayBlock(target);
            ChangePossibleOthersBeliefsAboutMe(isGoodAct);
            Debug.Log($"Gracz {PlayerNumber} zablokowal gracza {target.PlayerNumber} ");
        }

        protected void PlayRuin(FieldViewModel target, float isGoodAct)
        {
            Hand.PlayRuin(target);
            ChangePossibleOthersBeliefsAboutMe(isGoodAct);
            Debug.Log($"Gracz {PlayerNumber} zburzył tunel na ({target.X}, {target.Y})");
        }

        protected void PlayTunnel(FieldViewModel target, CardViewModel card, float isGoodAct)
        {
            Hand.PlayTunnel(target, card);
            ChangePossibleOthersBeliefsAboutMe(isGoodAct);
        }

        protected void DropSomeCard()
        {
            CardViewModel card = PickCardToDrop();
            Hand.DropCard(card);
            Debug.Log($"Gracz {PlayerNumber} wyrzuca kartę {card}");
        }

        protected abstract CardViewModel PickCardToDrop();
        protected abstract FieldViewModel FindTunnelFieldToRuin();

        protected int DistanceToGold()
        {
            return GameboardGrid.GetMinDistanceToGold();
        }

        protected void ChangePossibleOthersBeliefsAboutMe(float levelOfGoodAct)
        {
            ChangePossibleOthersBeliefsAbout(this, levelOfGoodAct);
        }

        protected void ChangePossibleOthersBeliefsAbout(PlayerViewModel actedPlayer, float levelOfGoodAct)
        {
            ChangeBelief(WhatIThinkLeftPlayerThinks, actedPlayer, levelOfGoodAct);
            ChangeBelief(WhatIThinkRightPlayerThinks, actedPlayer, levelOfGoodAct);
        }

        public void ChangeAllBeliefsAbout(PlayerViewModel actedPlayer, float levelOfGoodAct)
        {
            ChangeBelief(WhatIThink, actedPlayer, levelOfGoodAct);

            if (actedPlayer.PlayerNumber != GetLeftPlayer().PlayerNumber)
                ChangeBelief(WhatIThinkLeftPlayerThinks, actedPlayer, levelOfGoodAct);

            if (actedPlayer.PlayerNumber != GetRightPlayer().PlayerNumber)
                ChangeBelief(WhatIThinkRightPlayerThinks, actedPlayer, levelOfGoodAct);
        }

        protected void ChangeBelief(KripkeViewModel WhoThinks, PlayerViewModel actedPlayer, float levelOfGoodAct)
        {
            bool isSaboteur = ThinksPlayerIsSaboteur(WhoThinks, actedPlayer.PlayerNumber);
            WhoThinks.ChangeBelief(actedPlayer, isSaboteur, levelOfGoodAct);
        }

        protected PlayerViewModel GetLeftPlayer()
        {
            return GameManager.Players[(PlayerNumber + 1) % 3];
        }

        protected PlayerViewModel GetRightPlayer()
        {
            return GameManager.Players[(PlayerNumber + 2) % 3];
        }

        protected int GetLeftPlayerNumber()
        {
            return (PlayerNumber + 1) % 3;
        }

        protected int GetRightPlayerNumber()
        {
            return (PlayerNumber + 2) % 3;
        }

        protected float GetAnalizedLevelOfGoodAct(CardViewModel card)
        {
            float levelOfGoodAct = NeutralAct;
            int newDistanceToGold = DistanceToGold();
            switch (card.Type)
            {
                case CardType.Block:
                {
                    PlayerViewModel blockedPlayer = card.OnPlayer;
                    if (ThinksPlayerIsSaboteur(WhatIThink, blockedPlayer.PlayerNumber))
                        levelOfGoodAct = 1.2f;
                    else
                        levelOfGoodAct = 0.5f; //srednio zly ruch
                    break;
                }
                case CardType.Ruin:
                {
                    if (lastDistanceToGold > newDistanceToGold)
                        levelOfGoodAct = 0.1f; //bardzo zly ruch
                    else
                        levelOfGoodAct = 1.5f; //dobry ruch
                    break;
                }

                case CardType.Tunnel:
                {
                    if (lastDistanceToGold > newDistanceToGold ||
                        (card as TunnelCardViewModel).TunnelPattern.Matches(new TunnelPattern {DeadEnd = true}))
                        levelOfGoodAct = 0.13f; // zly ruch
                    else
                        levelOfGoodAct = 1.3f; //dobry ruch

                    break;
                }
                case CardType.Unblock:
                {
                    PlayerViewModel unblockedPlayer = card.OnPlayer;
                    if (unblockedPlayer == this)
                        levelOfGoodAct = GoodAct;
                    else
                        levelOfGoodAct = 0.3f; //srednio zly ruch
                    break;
                }
            }

            lastDistanceToGold = newDistanceToGold;
            return levelOfGoodAct;
        }

        protected Tuple<CardViewModel, FieldViewModel> FindGoodTunnel()
        {
            List<FieldViewModel> possibleFields = GameboardGrid.GetAllPossibleFields();
            List<FieldViewModel> fieldList = GameboardGrid.GetListOfTunnelsClosestToGold();

            foreach (var field in fieldList)
            {
                TunnelPattern occupyingCardPattern =
                    (field.OccupyingCard as TunnelCardViewModel)?.TunnelPattern;

                if (occupyingCardPattern.Up == Yes)
                {
                    TunnelPattern wantedTunnelPattern = new TunnelPattern {Down = Yes,  DeadEnd = false};
                    FieldViewModel whereToPlaceIt =
                        GameboardGrid.GetField(field.X, field.Y - 1);
                    if (possibleFields.Contains(whereToPlaceIt))
                    {
                        var tuple = TryPuttingCardOnField(wantedTunnelPattern, whereToPlaceIt, field);
                        if (tuple != null) return tuple;
                    }
                }

                if (occupyingCardPattern.Right == Yes)
                {
                    TunnelPattern wantedTunnelPattern = new TunnelPattern {Left = Yes,  DeadEnd = false};
                    FieldViewModel whereToPlaceIt =
                        GameboardGrid.GetField(field.X + 1, field.Y);
                    if (possibleFields.Contains(whereToPlaceIt))
                    {
                        var tuple = TryPuttingCardOnField(wantedTunnelPattern, whereToPlaceIt, field);
                        if (tuple != null) return tuple;
                    }
                }

                if (occupyingCardPattern.Down == Yes)
                {
                    TunnelPattern wantedTunnelPattern = new TunnelPattern {Up = Yes, DeadEnd = false};
                    FieldViewModel whereToPlaceIt =
                        GameboardGrid.GetField(field.X, field.Y + 1);
                    if (possibleFields.Contains(whereToPlaceIt))
                    {
                        var tuple = TryPuttingCardOnField(wantedTunnelPattern, whereToPlaceIt, field);
                        if (tuple != null) return tuple;
                    }
                }

                if (occupyingCardPattern.Left == Yes)
                {
                    TunnelPattern wantedTunnelPattern = new TunnelPattern {Right = Yes, DeadEnd = false};
                    FieldViewModel whereToPlaceIt =
                        GameboardGrid.GetField(field.X, field.Y + 1);
                    if (possibleFields.Contains(whereToPlaceIt))
                    {
                        var tuple = TryPuttingCardOnField(wantedTunnelPattern, whereToPlaceIt, field);
                        if (tuple != null) return tuple;
                    }
                }
            }

            return null;
        }

        private Tuple<CardViewModel, FieldViewModel> TryPuttingCardOnField(TunnelPattern wantedTunnelPattern,
            FieldViewModel whereToPlaceIt, FieldViewModel sourceField)
        {
            foreach (var card in Hand.Cards)
            {
                if (card is TunnelCardViewModel tunnelCard)
                {
                    if (tunnelCard.TunnelPattern.Matches(wantedTunnelPattern))
                    {
                        int minimumDistance = GameboardGrid.GetMinimumDistanceByPlacingCard(whereToPlaceIt, tunnelCard);

                        if (GameboardGrid.CanPlaceCardOnField(tunnelCard, whereToPlaceIt)
                            && minimumDistance <
                            GameboardGrid.GetDistanceBetweenFields(sourceField, FinishCard.OccupiedField))
                            return Tuple.Create((CardViewModel)tunnelCard, whereToPlaceIt);
                    }
                }
                
            }
            return null;
        }

        protected bool ThinksPlayerIsSaboteur(KripkeViewModel WhoThinks, int playerNumber)
        {
            return WhoThinks.GetWorld(playerNumber).Belief > 0.6;
        }

        protected float LevelOfGoodAct(CardViewModel card, int playerNumber)
        {
            if (card.Type == CardType.Begin)
            {
                lastDistanceToGold =
                    GameManager.GameboardGrid.GetDistanceBetweenFields(GameManager.StartCard.OccupiedField,
                        GameManager.FinishCard.OccupiedField);
                return -1;
            }

            lastDistanceToGold = DistanceToGold();
            float levelOfGoodAct = GetAnalizedLevelOfGoodAct(card);
            return levelOfGoodAct;
        }
    }
}