﻿using System;
using UnityEngine;
using static Saboteur.CardType;
using static Saboteur.GameManager;

namespace Saboteur
{
    public class MinerBotViewModel : BotViewModel
    {
        public MinerBotViewModel(int playerNumber) : base(false, playerNumber)
        {
            SetPlayerType(playerNumber);
            CreateKripkeModels(false);
        }

        private void SetPlayerType(int playerNumber)
        {
            Personality = playerNumber % 2 == 0 ? PlayerPersonality.Positive : PlayerPersonality.Negative; 
//            Personality = PlayerPersonality.Negative;
        }

        protected override void Play()
        {
            if (IsBlocked && HasCard(Unblock))
            {
                PlayUnblock(this, NeutralAct);
                return;
            }

            PlayerViewModel saboteur = FindSaboteur();
            if (saboteur != null && HasCard(Block) && !saboteur.IsBlocked && DistanceToGold() > 2)
            {
                PlayBlock(saboteur, GoodAct);
                return;
            }

            FieldViewModel badTunnel = FindTunnelFieldToRuin();
            if (badTunnel != null && HasCard(Ruin))
            {
                PlayRuin(badTunnel, GoodAct);
                return;
            }
            
            Tuple<CardViewModel, FieldViewModel> goodTunnelInfo = FindGoodTunnel();

            if (goodTunnelInfo != null)
            {
                CardViewModel goodTunnel = goodTunnelInfo.Item1;
                FieldViewModel field = goodTunnelInfo.Item2;
                
                if (!IsBlocked)
                {
                    PlayTunnel(field, goodTunnel, GoodAct);
                    return;
                }
            }

            PlayerViewModel blockedMiner = FindBlockedMiner();
            if (blockedMiner != null && HasCard(Unblock) && !blockedMiner.IsBlocked)
            {
                PlayUnblock(blockedMiner, GoodAct);
                return;
            }

            DropSomeCard();
        }

        public override void AnalyzePlayersAction(CardViewModel card, int playerNumber)
        {
            float levelOfGoodAct = LevelOfGoodAct(card, playerNumber);
            if (levelOfGoodAct < 0) return;
            
            PlayerViewModel actedPlayer = GameManager.Players[playerNumber];
            ChangeAllBeliefsAbout(actedPlayer, levelOfGoodAct);
        }

        private PlayerViewModel FindSaboteur()
        {
            int rightPlayerNumber = (PlayerNumber + 2) % 3;
            int leftPlayerNumber = (PlayerNumber + 1) % 3;

            if (ThinksPlayerIsSaboteur(WhatIThink, rightPlayerNumber)) return GameManager.Players[rightPlayerNumber];
            if (ThinksPlayerIsSaboteur(WhatIThink, leftPlayerNumber)) return GameManager.Players[leftPlayerNumber];

            return null;
        }

        private PlayerViewModel FindBlockedMiner()
        {
            int rightPlayerNumber = (PlayerNumber + 2) % 3;
            int leftPlayerNumber = (PlayerNumber + 1) % 3;

            if (ThinksPlayerIsSaboteur(WhatIThink, rightPlayerNumber) &&
                GameManager.Players[leftPlayerNumber].IsBlocked)
                return GameManager.Players[leftPlayerNumber];

            if (ThinksPlayerIsSaboteur(WhatIThink, leftPlayerNumber) &&
                GameManager.Players[rightPlayerNumber].IsBlocked)
                return GameManager.Players[rightPlayerNumber];

            return null;
        }

        protected override CardViewModel PickCardToDrop()
        {
            if (Hand.Cards.Count == 0) return null;
            
            CardViewModel bestCardToDrop = Hand.Cards[0];
            int deadEndCount = 0, blockCount = 0, unblockCount = 0, ruinCount = 0, tunnelCount = 0;
            foreach (CardViewModel card in Hand.Cards)
            {
                switch (card.Type)
                {
                    case CardType.Tunnel:
                        if (((TunnelCardViewModel) card).TunnelPattern.DeadEnd)
                            return card;

                        else
                        {
                            ++tunnelCount;
                            if (IsBlocked) bestCardToDrop = card;
                            if (tunnelCount == 2)
                                return card;
                        }
                        break;

                    case CardType.Block:
                        ++blockCount;
                        if (bestCardToDrop == null) bestCardToDrop = card;
                        if (blockCount == 2)
                            return card;
                        break;

                    case CardType.Unblock:
                        ++unblockCount;
                        if (unblockCount == 2)
                            return card;
                        break;

                    case CardType.Ruin:
                        ++ruinCount;
                        if (ruinCount == 2)
                            return card;
                        break;
                }
            }

            return bestCardToDrop;
        }

        

        protected override FieldViewModel FindTunnelFieldToRuin()
        {
            return GameboardGrid.GetDeadEndClosestToGold();
        }
    }
}