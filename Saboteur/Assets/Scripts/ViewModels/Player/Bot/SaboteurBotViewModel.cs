﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using static Saboteur.CardType;
using static Saboteur.Present;
using static Saboteur.GameManager;

namespace Saboteur
{
    public class SaboteurBotViewModel : BotViewModel
    {
        public SaboteurBotViewModel(int playerNumber) : base(true, playerNumber)
        {
            Personality = PlayerPersonality.Neutral;
            CreateKripkeModels(true);
        }

        protected override void Play()
        {
            if (IsBlocked)
            {
                if (HasCard(Unblock))
                {
                    PlayUnblock(this, NeutralAct);
                    return;
                }

                TryBlockMiner();
                return;
            }

            if (!IsBlocked)
            {
                int distanceToGold = DistanceToGold();
                if (distanceToGold < 13)
                {
                    Tuple<CardViewModel, FieldViewModel> badTunnel = FindBadTunnel();
                    if (badTunnel != null)
                    {
                        PlayTunnel(badTunnel.Item2, badTunnel.Item1, 0.1f);
                        return;
                    }

                    if (TryBlockMiner()) return;
                }
                
                else 
                {
                    if (!OthersThinkIAmSaboteur() && distanceToGold < 17)
                         if (TryBlockMiner()) return;
                    
                    Tuple<CardViewModel, FieldViewModel> goodTunnelInfo = FindGoodTunnel();
                    CardViewModel goodTunnel = goodTunnelInfo?.Item1;
                    FieldViewModel goodField = goodTunnelInfo?.Item2;
                    if (goodTunnel != null)
                    {
                        PlayTunnel(goodField, goodTunnel, GoodAct);
                        return;
                    }
                }
            }
            DropSomeCard();
        }

        private bool TryBlockMiner()
        {
            if (HasCard(Block))
            {
                PlayerViewModel rightPlayer = GetRightPlayer();
                PlayerViewModel leftPlayer = GetLeftPlayer();

                if (!rightPlayer.IsBlocked)
                {
                    PlayBlock(rightPlayer, 0.7f); //średnio zły ruch w oczach innych graczy
                    return true;
                }

                if (!leftPlayer.IsBlocked)
                {
                    PlayBlock(leftPlayer, 0.7f);
                    return true;
                }
            }

            return false;
        }

        private bool OthersThinkIAmSaboteur()
        {
            return ThinksPlayerIsSaboteur(WhatIThinkLeftPlayerThinks, PlayerNumber)
                   && ThinksPlayerIsSaboteur(WhatIThinkRightPlayerThinks, PlayerNumber);
        }

        private Tuple<CardViewModel, FieldViewModel> FindBadTunnel()
        {
            List<FieldViewModel> possibleFields = GameboardGrid.GetAllPossibleFields();
            List<FieldViewModel> fieldList =
                GameboardGrid.GetListOfTunnelsClosestToGold();

            foreach (var field in fieldList)
            {
                TunnelPattern occupyingCardPattern = (field.OccupyingCard as TunnelCardViewModel)?.TunnelPattern;
                Tuple<CardViewModel, FieldViewModel> wantedDeadEnd = FindDeadEnd(field, occupyingCardPattern);

                if (wantedDeadEnd != null)
                    return wantedDeadEnd;

                if (occupyingCardPattern.Right == Yes)
                {
                    TunnelPattern wantedTunnelPattern = new TunnelPattern
                        {Left = Yes, DeadEnd = false};
                    FieldViewModel whereToPlaceIt =
                        GameboardGrid.GetField(field.X + 1, field.Y);
                    if (possibleFields.Contains(whereToPlaceIt))
                    {
                        var tuple = ActIncreasesDistanceToGold(wantedTunnelPattern, whereToPlaceIt, field);
                        if (tuple != null) return tuple;
                    }
                }

                if (occupyingCardPattern.Up == Yes)
                {
                    TunnelPattern wantedTunnelPattern = new TunnelPattern
                        {Down = Yes, DeadEnd = false};
                    FieldViewModel whereToPlaceIt =
                        GameboardGrid.GetField(field.X, field.Y - 1);
                    if (possibleFields.Contains(whereToPlaceIt))
                    {
                        var tuple = ActIncreasesDistanceToGold(wantedTunnelPattern, whereToPlaceIt, field);
                        if (tuple != null) return tuple;
                    }
                }
            }

            return null;
        }

        private Tuple<CardViewModel, FieldViewModel> FindDeadEnd(FieldViewModel field,
            TunnelPattern occupyingCardPattern)
        {
            List<FieldViewModel> possibleFields = GameboardGrid.GetAllPossibleFields();

            TunnelPattern wantedTunnelPattern;
            if (occupyingCardPattern.Up == Present.Yes)
            {
                wantedTunnelPattern = new TunnelPattern {Down = Present.Yes, DeadEnd = true};
                CardViewModel wantedCard = Hand.FindTunnelWithPattern(wantedTunnelPattern);
                if (wantedCard != null)
                {
                    FieldViewModel checkedField = GameboardGrid.GetField(field.X + 1, field.Y);
                    if (possibleFields.Contains(checkedField))
                        return Tuple.Create(wantedCard, GameboardGrid.GetField(field.X, field.Y - 1));
                }
            }

            if (occupyingCardPattern.Right == Present.Yes)
            {
                wantedTunnelPattern = new TunnelPattern {Left = Present.Yes, DeadEnd = true};
                CardViewModel wantedCard = Hand.FindTunnelWithPattern(wantedTunnelPattern);
                if (wantedCard != null)
                {
                    FieldViewModel checkedField = GameboardGrid.GetField(field.X + 1, field.Y);
                    if (possibleFields.Contains(checkedField))
                        return Tuple.Create(wantedCard, checkedField);
                }
            }

            return null;
        }

        private Tuple<CardViewModel, FieldViewModel> ActIncreasesDistanceToGold(TunnelPattern wantedTunnelPattern,
            FieldViewModel whereToPlaceIt, FieldViewModel sourceField)
        {
            CardViewModel wantedCard = Hand.FindTunnelWithPattern(wantedTunnelPattern);
            if (wantedCard != null)
            {
                int minimumDistance = GameboardGrid.GetMinimumDistanceByPlacingCard(whereToPlaceIt, wantedCard);

                if (GameboardGrid.CanPlaceCardOnField(wantedCard, whereToPlaceIt)
                    && minimumDistance > GameboardGrid.GetDistanceBetweenFields(sourceField, FinishCard.OccupiedField))
                    return Tuple.Create(wantedCard, whereToPlaceIt);
            }

            return null;
        }

        protected override CardViewModel PickCardToDrop()
        {
            if (Hand.Cards.Count == 0) return null;

            CardViewModel bestCardToDrop = Hand.Cards[0];
            int tunnelCount = 0, blockCount = 0, unblockCount = 0, ruinCount = 0, deadEndCount = 0;
            foreach (CardViewModel card in Hand.Cards)
            {
                switch (card.Type)
                {
                    case CardType.Tunnel:
                        if (!((TunnelCardViewModel) card).TunnelPattern.DeadEnd)
                        {
                            ++tunnelCount;
                            if (tunnelCount == 2)
                                bestCardToDrop = card;
                        }
                        else
                        {
                            ++deadEndCount;
                            if (deadEndCount == 3)
                                return card;
                        }
                        break;

                    case CardType.Block:
                        ++blockCount;
                        if (bestCardToDrop == null) bestCardToDrop = card;
                        if (blockCount == 2)
                            return card;
                        break;

                    case CardType.Unblock:
                        ++unblockCount;
                        if (unblockCount == 2)
                            return card;
                        break;

                    case CardType.Ruin:
                        ++ruinCount;
                        if (ruinCount == 2)
                            return card;
                        break;
                }
            }

            return bestCardToDrop;
        }


        protected override FieldViewModel FindTunnelFieldToRuin()
        {
            throw new System.NotImplementedException();
        }

        public override void AnalyzePlayersAction(CardViewModel card, int playerNumber)
        {
            float levelOfGoodAct = LevelOfGoodAct(card, playerNumber);
            if (levelOfGoodAct < 0) return;
            PlayerViewModel actedPlayer = GameManager.Players[playerNumber];

            if (playerNumber != GetLeftPlayer().PlayerNumber)
                ChangeBelief(WhatIThinkLeftPlayerThinks, actedPlayer, levelOfGoodAct);

            if (playerNumber != GetRightPlayer().PlayerNumber)
                ChangeBelief(WhatIThinkRightPlayerThinks, actedPlayer, levelOfGoodAct);
        }
    }
}