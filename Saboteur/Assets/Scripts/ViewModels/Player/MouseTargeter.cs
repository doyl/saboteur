﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTargeter : MonoBehaviour
{
    public static MouseTargeter instance;

    private void Start()
    {
        instance = this;
    }

    private void Update()
    {
    //    if (Input.GetMouseButtonDown(0))
    //        if(GetTargetUnderMouse()!=null) Debug.Log("Yeah!");
    }

    public CardTargetView GetTargetUnderMouse()
    {
        Ray ray;
        RaycastHit hit;

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            //print(hit.collider.name);
            GameObject go = hit.transform.gameObject;
            return go.GetComponent<CardTargetView>();
        }

        return null;
    }
}
