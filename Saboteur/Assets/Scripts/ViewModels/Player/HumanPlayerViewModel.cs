namespace Saboteur
{
    public class HumanPlayerViewModel: PlayerViewModel
    {
        public HumanPlayerViewModel(bool isSaboteur, int playerNumber) : base(isSaboteur, playerNumber)
        {
        }

        public override void BeginTurn()
        {
            CardView.IsDraggingAllowed = true;
        }

        public void TakeCard()
        {
            Hand.TakeCard();
        }
    }
}