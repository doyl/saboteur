using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine.Experimental.Rendering;

namespace Saboteur
{
    public abstract class PlayerViewModel: ICardTarget
    {
        private ISubject<CardViewModel> cardUsedSubject = new Subject<CardViewModel>();
        public IObservable<CardViewModel> CardUsedStream => Hand.CardUsedStream;

        private Subject<bool> blockedSubject = new Subject<bool>();
        public IObservable<bool> BlockedStream => blockedSubject.AsObservable();
        
        private bool isBlocked;
        public bool IsBlocked
        {
            get
            {
                return isBlocked;
            }
            set
            {
                isBlocked = value;
                blockedSubject.OnNext(isBlocked);
            } 
        }
        public bool IsSaboteur { get; }
        public int PlayerNumber { get; set; }
        public PlayerPersonality Personality { get; set; }
        
        public HandViewModel Hand { get; private set; }
        public KripkeViewModel WhatIThink { get; protected set; }
        public KripkeViewModel WhatIThinkLeftPlayerThinks { get; protected set; }
        public KripkeViewModel WhatIThinkRightPlayerThinks { get; protected set; }

        protected PlayerViewModel(bool isSaboteur, int playerNumber)
        {
            IsSaboteur = isSaboteur;
            PlayerNumber = playerNumber;
            IsBlocked = false;
            Hand = new HandViewModel(this);
            TakeAllCards();
        }
        
        public abstract void BeginTurn();

        public void TakeAllCards()
        {
            for (int i = 0; i < 6; ++i)
                Hand.TakeCard();
        }
    }
}