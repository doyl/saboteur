using System.CodeDom;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using UnityScript.Macros;
//using Random = System.Random;
using static Saboteur.Present;

namespace Saboteur
{
    public class DeckViewModel
    {
        public List<CardViewModel> Cards { get; }
        
        private const int BlockCardsNumber = 7;
        private const int UnblockCardsNumber = 7;
        private const int RuinCardsNumber = 3;
        private const int TunnelCardsNumber = 45;
        private const int DeckCardsNumber = 62;
        
        public DeckViewModel()
        {
            Cards = new List<CardViewModel>();
            
            GenerateBlockList();
            GenerateUnblockList();
            GenerateRuinList();
            GenerateCards();

            Shuffle();
        }

        private void GenerateBlockList()
        {
            for (int i = 0; i < BlockCardsNumber; ++i)
                Cards.Add(new BlockCardViewModel());
        }
        
        private void GenerateUnblockList()
        {
            for (int i = 0; i < UnblockCardsNumber; ++i)
                Cards.Add(new UnblockCardViewModel());
        }
        
        private void GenerateRuinList()
        {
            for (int i = 0; i < RuinCardsNumber; ++i)
                Cards.Add(new RuinCardViewModel());
        }

        private void GenerateCards()
        {
            GenerateTunnels();
            GenerateDeadEnds();
        }

        private void GenerateTunnels()
        {

            for (int i = 0; i < 10; ++i)
            {
            //    Cards.Add(new TunnelCardViewModel(Yes, Yes, Yes, Yes, false)); //krzyz
                Cards.Add(new TunnelCardViewModel(Yes, Yes, Yes, No, false)); //bez prawego/lewego
                Cards.Add(new TunnelCardViewModel(Yes, No, Yes, Yes, false)); //bez gory/dola
                Cards.Add(new TunnelCardViewModel(No, No, Yes, Yes, false)); //right-left
                Cards.Add(new TunnelCardViewModel(Yes, No, No, Yes, false)); //up-left    ???      
                Cards.Add(new TunnelCardViewModel(No, Yes, Yes, No, false)); //down-right
                Cards.Add(new TunnelCardViewModel(Yes, Yes, No, No, false)); //up-down
            } 
        }

        private void GenerateDeadEnds()
        {
            for (int i = 0; i< 12; ++i) 
            /*    Cards.Add(new TunnelCardViewModel(Yes, Yes, Yes, Yes, true)); //krzyz
            
            Cards.Add(new TunnelCardViewModel(No, Yes, Yes, No, true)); //down-left
            Cards.Add(new TunnelCardViewModel(No, Yes, No, Yes, true)); //down-right
            Cards.Add(new TunnelCardViewModel(Yes, Yes, No, No, true)); //down-up*/
            
            Cards.Add(new TunnelCardViewModel(No, No, No, Yes, true)); //left/right
            Cards.Add(new TunnelCardViewModel(Yes, No, No, No, true)); //down/up

            /*Cards.Add(new TunnelCardViewModel(Yes, Yes, No, Yes, true)); //down-up-left/right
            Cards.Add(new TunnelCardViewModel(No, No, Yes, Yes, true)); //left-right
            Cards.Add(new TunnelCardViewModel(No, Yes, Yes, Yes, true)); //left-down-up/down*/

        }

        public bool HasNoMoreCards()
        {
            return Cards.Count == 0;
        }

        public CardViewModel GetCard()
        {
            if (Cards.Count == 0)
            {
                Debug.Log("Koniec kart");
                return null;
            }
            
            CardViewModel lastCard = Cards.Last();
            if (lastCard == null) return null;
            Cards.Remove(lastCard);
            return lastCard;
        }

        private void Shuffle()
        {
            int n = Cards.Count;
            while (n > 1)
            {
                n--;
                int k = Random.Range(0, n + 1);
                CardViewModel value = Cards[k];
                Cards[k] = Cards[n];
                Cards[n] = value;
            }
        }
    }
}