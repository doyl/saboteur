﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Saboteur;
using UniRx;
using System;

public class FieldViewModel : ICardTarget
{
    public int X { get; set; }
    public int Y { get; set; }
    
    public CardViewModel OccupyingCard { get; private set; }

    private Subject<CardViewModel> cardPlacedSubject = new Subject<CardViewModel>();
    public IObservable<CardViewModel> CardPlacedStream => cardPlacedSubject.AsObservable();
    
    private Subject<Unit> cardRemovedSubject = new Subject<Unit>();
    public IObservable<Unit> CardRemovedStream => cardRemovedSubject.AsObservable();

    public void PutCard(CardViewModel card)
    {
        OccupyingCard = card;
        cardPlacedSubject.OnNext(card);
    }

    public void RemoveCard()
    {
        OccupyingCard = null;
        cardRemovedSubject.OnNext(Unit.Default);
    }

    public bool isEmpty()
    {
        return OccupyingCard == null;
    }

    public bool Contains(CardType type)
    {
        return OccupyingCard != null && OccupyingCard.Type == type;
    }

    public CardViewModel GetCard()
    {
        return OccupyingCard;
    }

    public void Clear()
    {
        if (OccupyingCard == null) return;

        OccupyingCard.Remove();
        OccupyingCard = null;
    }

}
