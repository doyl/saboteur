using System;
using UniRx;

namespace Saboteur
{
    public abstract class CardViewModel
    {
        public CardType Type { get; protected set; }
        public FieldViewModel OccupiedField { get; set; }
        public PlayerViewModel OnPlayer { get; set; }
        public PlayerViewModel SourcePlayer { get; set; }
        

        
        #region Streams
        
        private readonly Subject<CardViewModel> cardUsedSubject = new Subject<CardViewModel>();
        public IObservable<CardViewModel> CardUsedStream => cardUsedSubject.AsObservable();

        private readonly Subject<Unit> cardRemovedSubject = new Subject<Unit>();
        public IObservable<Unit> CardRemovedStream { get { return cardRemovedSubject.AsObservable(); } }
        
        #endregion

        protected CardViewModel(CardType type)
        {
            Type = type;
        }

        public void Use(PlayerViewModel sourcePlayer, ICardTarget target)
        {
            cardUsedSubject.OnNext(this);
            SourcePlayer = sourcePlayer;
            DoUse(target);
        }

        public void Remove()
        {
            cardRemovedSubject.OnNext(Unit.Default);
        }

        protected abstract void DoUse(ICardTarget target);
    }
}