namespace Saboteur
{
    public class FinishCardViewModel : TunnelCardViewModel
    {
        public FinishCardViewModel() : base(Present.Yes, Present.Yes, Present.Yes, Present.Yes, false)
        {
            Type = CardType.Gold;
        }
    }
}