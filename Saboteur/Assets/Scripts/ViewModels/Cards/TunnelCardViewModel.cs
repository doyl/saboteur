using UnityEngine;

using static Saboteur.Present;

namespace Saboteur
{
    public class TunnelCardViewModel : CardViewModel
    {
        public TunnelPattern TunnelPattern { get; } = new TunnelPattern();

        public TunnelCardViewModel(
            Present up = DontCare, 
            Present down = DontCare, 
            Present right = DontCare, 
            Present left = DontCare, 
            bool deadEnd = false): base(CardType.Tunnel)
        {
            TunnelPattern.Up = up;
            TunnelPattern.Down = down;
            TunnelPattern.Left = left;
            TunnelPattern.Right = right;
            TunnelPattern.DeadEnd = deadEnd;
        }

        protected override void DoUse(ICardTarget target)
        {           
            if(target is FieldViewModel field)
            {
                Debug.Log($"Zagrano tunel na polu ({field.X}, {field.Y})");
                OccupiedField = field;
                GameManager.GameboardGrid.PutTunnel(field, this);
            }
            else
            {
                Debug.Log("Target for tunnel card must be field!");
            }
        }
    }
}