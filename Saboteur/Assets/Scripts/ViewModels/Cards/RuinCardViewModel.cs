using UnityEngine;

namespace Saboteur
{
    public class RuinCardViewModel : CardViewModel
    {
        public RuinCardViewModel(): base(CardType.Ruin) 
        {}
        
        protected override void DoUse(ICardTarget target)
        {
            Debug.Log("Ruin");

            if (target is FieldViewModel field)
            {
                GameManager.GameboardGrid.RuinTunnel(field);                
            }
            else
            {
                Debug.Log("Target for ruin card must be field!");
            }
        }
    }
}