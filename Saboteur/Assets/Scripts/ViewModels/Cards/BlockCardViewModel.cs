using System;
using UnityEngine;

namespace Saboteur
{
    public class BlockCardViewModel : CardViewModel
    {
        public BlockCardViewModel() : base(CardType.Block)
        {
        }

        protected override void DoUse(ICardTarget target)
        {
            Debug.Log("Block");
            
            if (target is PlayerViewModel player)
            {
                player.IsBlocked = true;
                OnPlayer = player;
            }
            else
            {
//                throw new ArgumentException("Target for block card must be a player");
                Debug.Log("Target for block card must be a player!");
            }
        }
    }
}