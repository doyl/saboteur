using System;
using UnityEngine;

namespace Saboteur
{
    public class UnblockCardViewModel : CardViewModel
    {
        public UnblockCardViewModel(): base(CardType.Unblock)
        {}
        
        protected override void DoUse(ICardTarget target)
        {
            Debug.Log("Unblock");
            
            if (target is PlayerViewModel player)
            {
                player.IsBlocked = false;
                OnPlayer = player;
            }
            else
            {
//                throw new ArgumentException("Target for unblock card must be a player");
                Debug.Log("Target for unblock card must be a player!");
            }
        }
    }
}