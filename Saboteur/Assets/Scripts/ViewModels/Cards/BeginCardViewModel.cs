using System;
using UniRx;
using UnityEngine;

namespace Saboteur
{
    public class BeginCardViewModel :TunnelCardViewModel
    {
        public BeginCardViewModel() : base(Present.Yes, Present.No, Present.Yes, Present.Yes, false)
        {
            Type = CardType.Begin;
        }
    }
}