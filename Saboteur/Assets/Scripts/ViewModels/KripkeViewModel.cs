using System;
using UniRx;
using System.Collections.Generic;
using UnityEngine;

namespace Saboteur
{
    public class KripkeViewModel
    {
        public int PlayerNumber { get; }
        public float Alpha { get; set; }
        public float Beta { get; set; }
        public List<GameWorld> GameWorlds { get; set; } = new List<GameWorld>();

        private static readonly float Hight = 0.13f;
        private static readonly float Average = 0.07f;
        private static readonly float Low = 0.05f;

        public readonly float NeutralAct = BotViewModel.NeutralAct;

        private BehaviorSubject<List<GameWorld>> beliefsChangedSubject;

        public IObservable<List<GameWorld>> BeliefsChangedObservable
        {
            get { return beliefsChangedSubject.AsObservable(); }
        }

        public KripkeViewModel(BotViewModel player, int playerNumber)
        {
            PlayerNumber = playerNumber;
            SetPlayersAlphaBeta(player.Personality);
        }

        public void SetModel(bool isWhatIThinkModel, bool isSaboteur)
        {
            if (isWhatIThinkModel)
                CreatePlayerModel(isSaboteur);
            else CreateAnotherPlayerModel();
            beliefsChangedSubject = new BehaviorSubject<List<GameWorld>>(GameWorlds);
            
        }

        private void CreatePlayerModel(bool isSaboteur)
        {
            if (isSaboteur) CreateSaboteurModel();
            else CreateMinerModel();
        }

        private void SetPlayersAlphaBeta(PlayerPersonality personality)
        {
            if (personality == PlayerPersonality.Positive)
            {
                Alpha = Hight;
                Beta = Low;
            }

            if (personality == PlayerPersonality.Negative)
            {
                Alpha = Low;
                Beta = Hight;
            }

            if (personality == PlayerPersonality.Neutral)
            {
                Alpha = Average;
                Beta = Average;
            }
        }

        private void CreateMinerModel()
        {
            for (int saboteurNumber = 0; saboteurNumber < 3; ++saboteurNumber)
            {
                float belief = (saboteurNumber == PlayerNumber) ? 0.0f : 0.5f;
                GameWorlds.Add(new GameWorld(saboteurNumber, belief));
            }
        }

        private void CreateSaboteurModel()
        {
            for (int saboteurNumber = 0; saboteurNumber < 3; ++saboteurNumber)
            {
                float belief = (saboteurNumber == PlayerNumber) ? 1.0f : 0.0f;
                GameWorlds.Add(new GameWorld(saboteurNumber, belief));
            }
        }

        private void CreateAnotherPlayerModel()
        {
            float belief = 0.5f;
            for (int i = 0; i < 3; ++i)
            {
                GameWorlds.Add(new GameWorld(i, belief));
            }
        }

        public GameWorld GetWorld(int PlayerNumber)
        {
            return GameWorlds[PlayerNumber];
        }

        public void ChangeBelief(PlayerViewModel actedPlayer, bool isSaboteur, float levelOfGoodAct)
        {
            int actedPlayerNumber = actedPlayer.PlayerNumber;
            bool isGoodAct = levelOfGoodAct > NeutralAct;

            if (levelOfGoodAct == NeutralAct) return;

            if (isSaboteur && !isGoodAct || !isSaboteur && isGoodAct)
            {
                AccordingToBeliefs(actedPlayerNumber, isSaboteur, levelOfGoodAct); 
            }

            if (isSaboteur && isGoodAct || !isSaboteur && !isGoodAct)
            {
                ContraryToBeliefs(actedPlayerNumber, isSaboteur, levelOfGoodAct); 
            }
            
            beliefsChangedSubject.OnNext(GameWorlds);
        }

        public void AccordingToBeliefs(int actedPlayerNumber, bool isSaboteur, float levelOfGoodAct)
        {
            int anotherPlayerNumber = FindAnotherPlayerNumber(actedPlayerNumber);
            float newBelief;
            
            if (isSaboteur)
            {
                float beliefThisPlayerIsSaboteur = GameWorlds[actedPlayerNumber].Belief;
                newBelief = beliefThisPlayerIsSaboteur + (1 - beliefThisPlayerIsSaboteur) * Alpha * levelOfGoodAct;  //𝑇𝐹𝑛=𝑇𝐹𝑛−1+(1−𝑇𝐹𝑛−1)*𝛼
                float beliefDifference = newBelief - beliefThisPlayerIsSaboteur;
                GameWorlds[actedPlayerNumber].ChangeBelief(beliefDifference);
                GameWorlds[anotherPlayerNumber].ChangeBelief(-beliefDifference);
            }

            else
            {
                float beliefPlayerIsMiner = GameWorlds[anotherPlayerNumber].Belief;
                newBelief = beliefPlayerIsMiner + (1 - beliefPlayerIsMiner) * Alpha * levelOfGoodAct; //𝑇𝐹𝑛=𝑇𝐹𝑛−1+(1−𝑇𝐹𝑛−1)*𝛼
                float beliefDifference = newBelief - beliefPlayerIsMiner;
                GameWorlds[anotherPlayerNumber].ChangeBelief(beliefDifference);
                GameWorlds[actedPlayerNumber].ChangeBelief(-beliefDifference);
            }
        }

        public void ContraryToBeliefs(int actedPlayerNumber, bool isSaboteur, float levelOfGoodAct)
        {
            int anotherPlayerNumber = FindAnotherPlayerNumber(actedPlayerNumber);
            float newBelief;
            
            if (isSaboteur)
            {
                float beliefThisPlayerIsSaboteur = GameWorlds[actedPlayerNumber].Belief;
                newBelief = beliefThisPlayerIsSaboteur - (1 - beliefThisPlayerIsSaboteur) * Beta * levelOfGoodAct; //𝑇𝐹𝑛=𝑇𝐹𝑛−1+(1−𝑇𝐹𝑛−1)*?𝛽
                float beliefDifference = newBelief - beliefThisPlayerIsSaboteur;
                GameWorlds[actedPlayerNumber].ChangeBelief(beliefDifference);
                GameWorlds[anotherPlayerNumber].ChangeBelief(-beliefDifference);
            }
            else
            {
                float beliefThisPlayerIsMiner = GameWorlds[anotherPlayerNumber].Belief;
                newBelief = beliefThisPlayerIsMiner - (1 - beliefThisPlayerIsMiner) * Beta * levelOfGoodAct; //𝑇𝐹𝑛=𝑇𝐹𝑛−1+(1−𝑇𝐹𝑛−1)*?𝛽
                float beliefDifference = newBelief - beliefThisPlayerIsMiner;
                GameWorlds[anotherPlayerNumber].ChangeBelief(beliefDifference);
                GameWorlds[actedPlayerNumber].ChangeBelief(-beliefDifference);
            }
        }

        int FindAnotherPlayerNumber(int actedPlayerNumber)
        {
            for (int number = 0; number < 3; ++number)
            {
                if (number != PlayerNumber && number != actedPlayerNumber)
                    return number;
            }

            return -1;
        }
    }
}