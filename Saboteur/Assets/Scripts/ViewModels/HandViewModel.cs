﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UniRx;
using UniRx.Async;
using UnityEngine;

namespace Saboteur
{
    public class HandViewModel
    {
        #region Streams

        private Subject<CardViewModel> cardTakenSubject = new Subject<CardViewModel>();
        public IObservable<CardViewModel> CardTakenStream => cardTakenSubject.AsObservable();

        private Subject<CardViewModel> cardUsedSubject = new Subject<CardViewModel>();
        public IObservable<CardViewModel> CardUsedStream => cardUsedSubject.AsObservable();

        private Subject<CardViewModel> cardRemovedSubject = new Subject<CardViewModel>();
        public IObservable<CardViewModel> CardRemovedStream => cardRemovedSubject.AsObservable();

        #endregion

        public List<CardViewModel> Cards { get; } = new List<CardViewModel>();
        private PlayerViewModel player;

        public HandViewModel(PlayerViewModel player)
        {
            this.player = player;
        }

        public void TakeCard()
        {
            CardViewModel card = GameManager.Deck.GetCard();
            if (card == null) return;
            Cards.Add(card);

            card.CardUsedStream.Subscribe(c =>
            {
                RemoveCard(card);
                cardUsedSubject.OnNext(c);
            });
            cardTakenSubject.OnNext(card);
        }

        private void RemoveCard(CardViewModel card)
        {
            Cards.Remove(card);
            cardRemovedSubject.OnNext(card);
        }

        public bool HasCard(CardType cardType)
        {
            return Cards.Any(card => card.Type == cardType);
        }

        public bool HasTunnelCard(TunnelPattern pattern)
        {
            return Cards.OfType<TunnelCardViewModel>().Any(card => card.TunnelPattern.Matches(pattern));
        }

        public bool HasNoMoreCards()
        {
            return Cards.Count == 0;
        }

        public CardViewModel FindTunnelWithPattern(TunnelPattern pattern)
        {
            if (HasTunnelCard(pattern))
                return Cards.OfType<TunnelCardViewModel>().First(c => c.TunnelPattern.Matches(pattern));

            return null;
        }

        public void DropCard(CardViewModel card)
        {
            RemoveCard(card);
            TakeCard();
            cardUsedSubject.OnNext(null);
        }

        public void PlayBlock(PlayerViewModel target)
        {
            try
            {
                CardViewModel card = Cards.First(c => c.Type == CardType.Block);
                card.Use(player, target);
                TakeCard();
            }
            catch (InvalidOperationException e)
            {
                throw new InvalidOperationException("this player has no block cards");
            }
        }

        public void PlayUnblock(PlayerViewModel target)
        {
            try
            {
                CardViewModel card = Cards.First(c => c.Type == CardType.Unblock);
                card.Use(player, target);
                TakeCard();
            }
            catch (InvalidOperationException e)
            {
                throw new InvalidOperationException("this player has no unblock cards");
            }
        }

        public void PlayRuin(FieldViewModel target)
        {
            try
            {
                CardViewModel card = Cards.First(c => c.Type == CardType.Ruin);
                card.Use(player, target);
                TakeCard();
            }
            catch (InvalidOperationException e)
            {
                //throw new InvalidOperationException("this player has no ruin cards");
            }
        }

        public void PlayTunnel(FieldViewModel target, CardViewModel card)
        {
            card.Use(player, target);
            TakeCard();
        }

        public void Clear()
        {
           while(Cards.Count>0)
            {
                RemoveCard(Cards[0]);
            }
        }
    }
}