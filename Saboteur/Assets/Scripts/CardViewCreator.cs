﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Saboteur;

public class CardViewCreator : MonoBehaviour
{
    [SerializeField] GameObject tunnelCardPrefab;
    public static CardViewCreator instance;

    [SerializeField] Sprite placeholderSprite;
    [SerializeField] Sprite blockCardSprite;
    [SerializeField] Sprite unblockCardSprite;
    [SerializeField] Sprite ruinCardSprtie;
    [SerializeField] Sprite goldSprite;

    [Header("Tunnels")]
    [SerializeField] Sprite tunnelUpDownCardSprite;
    [SerializeField] Sprite tunnelRightLeftCardSprite;
    [SerializeField] Sprite tunnelDownRightCardSprite;
    [SerializeField] Sprite tunnelDownLeftCardSprite;
    [SerializeField] Sprite tunnelDownRightLeftSprite;
    [SerializeField] Sprite tunnelUpDownLeftSprite;
    [SerializeField] Sprite tunnelUpDownRightLeftSprite;

    [Header("Roadends")]
    [SerializeField] Sprite roadendUpSprite;
    [SerializeField] Sprite roadendRightSprite;
    [SerializeField] Sprite roadendUpDownSprite;
    [SerializeField] Sprite roadendRightLeftSprite;
    [SerializeField] Sprite roadendDownLeftSprite;
    [SerializeField] Sprite roadendDownRightSprite;
    [SerializeField] Sprite roadendUpRightLeftSprite;
    [SerializeField] Sprite roadendUpDownRightSprite;
    [SerializeField] Sprite roadendUpDownRightLeftSprite;

    public CardView GetCardView(CardViewModel vm)
    {
        CardView card = Instantiate(tunnelCardPrefab).GetComponent<CardView>();
        card.SetViewModel(vm);
        return card;
    }

    private void Start()
    {
        instance = this;
    }

    public Sprite GetSpriteFor(CardViewModel card)
    {
        switch(card.Type)
        {
            case CardType.Begin:
                return tunnelUpDownRightLeftSprite;
            case CardType.Block:
                return blockCardSprite;
            case CardType.Unblock:
                return unblockCardSprite;
            case CardType.Gold:
                return goldSprite;
            case CardType.Ruin:
                return ruinCardSprtie;
            case CardType.Tunnel:
                return GetSpriteForTunnel(((TunnelCardViewModel)card).TunnelPattern);
            default:
                return placeholderSprite;
            
        }
    }

    public Sprite GetSpriteForTunnel(TunnelPattern pattern)
    {
        if (pattern.DeadEnd) return GetSpriteForDeadEnd(pattern);

        if (isPatterEqual(pattern, true, true, false, false))
            return tunnelUpDownCardSprite;
        if (isPatterEqual(pattern, true, true, false, false))
            return tunnelUpDownCardSprite;
        if (isPatterEqual(pattern, true, true, false, false))
            return tunnelUpDownCardSprite;
        if (isPatterEqual(pattern, false, false, true, true))
            return tunnelRightLeftCardSprite;
        if (isPatterEqual(pattern, false, true, true, false))
            return tunnelDownRightCardSprite;
        if (isPatterEqual(pattern, false, true, false, true))
            return tunnelDownLeftCardSprite;
        if (isPatterEqual(pattern, false, true, true, true))
            return tunnelDownRightLeftSprite;
        if (isPatterEqual(pattern, true, true, false, true))
            return tunnelUpDownLeftSprite;
        if (isPatterEqual(pattern, true, true, true, true))
            return tunnelUpDownRightLeftSprite;

        return null;
    }

    private Sprite GetSpriteForDeadEnd(TunnelPattern pattern)
    {
        if (isPatterEqual(pattern, true, false, false, false))
            return roadendUpSprite;
        if (isPatterEqual(pattern, false, false, true, false))
            return roadendRightSprite;
        if (isPatterEqual(pattern, true, true, false, false))
            return roadendUpDownSprite;
        if (isPatterEqual(pattern, false, false, true, true))
            return roadendRightLeftSprite;
        if (isPatterEqual(pattern, false, true, false, true))
            return roadendDownLeftSprite;
        if (isPatterEqual(pattern, false, true, true, false))
            return roadendDownRightSprite;
        if (isPatterEqual(pattern, true, false, true, true))
            return roadendUpRightLeftSprite;
        if (isPatterEqual(pattern, true, true, true, false))
            return roadendUpDownRightSprite;
        if (isPatterEqual(pattern, true, true, true, true))
            return roadendUpDownRightLeftSprite;

        return null;
    }

    private bool isPatterEqual(TunnelPattern pattern, bool up, bool down, bool right, bool left)
    {
        bool upOk = up ? pattern.Up == Present.Yes : pattern.Up == Present.No;
        bool downOk = down ? pattern.Down == Present.Yes : pattern.Down == Present.No;
        bool rightOk = right ? pattern.Right == Present.Yes : pattern.Right == Present.No;
        bool leftOk = left ? pattern.Left == Present.Yes : pattern.Left == Present.No;

        return upOk && downOk && rightOk && leftOk;
    }
}
