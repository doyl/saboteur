﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Saboteur;

public abstract class CardTargetView : MonoBehaviour
{
    public abstract ICardTarget getCardTarget();
}
