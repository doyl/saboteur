﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Saboteur;
using UniRx;

public class EndMenu : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI winnerText;
    [SerializeField] TextMeshProUGUI saboteursWinsText;
    [SerializeField] TextMeshProUGUI minersWinsText;

    public static EndMenu instance;

    public void Initialize()
    {
        instance = this;
        TurnManager.instance.PlayerWonStream.Merge(GameManager.GameboardGrid.PlayerWonStream).Subscribe(Win);
        Show(false);
    }

    private void Win(PlayerViewModel player)
    {
        if (player.IsSaboteur)
        {
            SetMinersWins(0);
            SetSaboteurWins(1);
        }
        else
        {
            SetSaboteurWins(0);
            SetMinersWins(1);
        }
        SetSaboteurWon(player.IsSaboteur);
        Show(true);
    }

    public void Show(bool show)
    {
        gameObject.SetActive(show);
    }

    public void SetSaboteurWon(bool won)
    {
        if (won)
            winnerText.text = "Saboteur won!";
        else
            winnerText.text = "Miners won!";
    }
    
    public void SetSaboteurWins(int wins)
    {
        saboteursWinsText.text = "Saboteurs wins: " + wins;
    }

    public void SetMinersWins(int wins)
    {
        minersWinsText.text = "Miners wins: " + wins;
    }
}
