﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Saboteur;

public class GameRestarter : MonoBehaviour
{
    public void RestartGame()
    {
        GameManager.Restart();
        //SceneManager.LoadScene(0);

    }
}
